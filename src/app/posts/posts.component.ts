import { Component, OnInit } from '@angular/core';
import { PostsService } from './posts.Service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  
  isLoading:Boolean = true;
  
  posts;
  
  currentPost;

  select(post){
  this.currentPost = post;
  }
  
  constructor(private _postsService:PostsService) {}
    
    deletePost(post){
      this.posts.splice(
        this.posts.indexOf(post),1
     )
   }

  ngOnInit() {
    this._postsService.getPosts().subscribe(postsData => {this.posts=postsData;
    this.isLoading=false});
  }

}
